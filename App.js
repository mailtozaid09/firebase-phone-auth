import React from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, StatusBar } from 'react-native'

import { NavigationContainer } from '@react-navigation/native'
import Navigator from './src/navigator/Navigator'
import { colors } from './src/utils/colors'

const App = () => {
    return (
        <NavigationContainer>
            <StatusBar backgroundColor={colors.black} />
            <Navigator />
        </NavigationContainer>
    )
}

export default App
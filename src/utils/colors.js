export const colors = {
    primary: '#fafafa',
    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
    black: '#333333',
    white: '#fdfdfd',
    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',
}
import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity } from 'react-native'
import { colors } from '../../utils/colors'

import auth from '@react-native-firebase/auth';
import { media } from '../../utils/media';

const HomeScreen = ({navigation}) => {

    const [userDetails, setUserDetails] = useState();

    useEffect(() => {
        auth().onAuthStateChanged((user) => {
            if(user) {
                setUserDetails(user)
                console.log('====================================');
                console.log("user authenticated => ", user);
                console.log('====================================');
            }
        })
    
    }, [])
    

    const logoutFunction = async () => {
        auth().signOut()
        .then((success) => {
            console.log("success --  LOG OUT");
            navigation.navigate('Login');
        }).catch((error) => {
            console.log("firebase error");
            console.log(error);
            alert(error)
        });
    }



    return (
        <SafeAreaView style={styles.container} >
            <Image 
                source={media.verified}
                style={styles.image}
            />
            <View>
                <Text style={styles.heading} >Welcome</Text>
                <Text style={styles.subheading} >{userDetails?.phoneNumber}</Text>
            </View>

            <TouchableOpacity
                    onPress={() => {logoutFunction()}}
                    disabled={false}
                    style={styles.loginButton}
                >
                    <Text style={styles.loginButtonText} >LOGOUT</Text>
            </TouchableOpacity> 
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    heading: {
        fontSize: 30,
        color: colors.black,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    subheading: {
        fontSize: 22,
        color: colors.black,
        textAlign: 'center',
        fontWeight: '600',
    },
    image: {
        height: 280,
        width: 280,
    },
    loginButton: {
        height: 60,
        borderRadius: 30,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: colors.black,
    },
    loginButtonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
    },
})

export default HomeScreen
import React, {useState, useEffect, useRef} from 'react'
import { Text, View, TextInput,  StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity } from 'react-native'

import auth from '@react-native-firebase/auth';

import { screenWidth } from '../../utils/constants';
import { colors } from '../../utils/colors';
import { media } from '../../utils/media';

import OTPTextInput from 'react-native-otp-textinput'



var timerVal = 60

const VerificationScreen = ({navigation, route}) => {

    let otpInput = useRef(null);

    const [number, setNumber] = useState(route.params.params);
    const [resendTimer, setResendTimer] = useState(10);
   
    const [otpValue, setOtpValue] = useState(null);
    const [confirm, setConfirm] = useState(null);
    
    useEffect(() => {
        sendOtpFunction()
        startResendTimer()
    }, [])

    const startResendTimer = () => {
        setResendTimer(timerVal)

        if(timerVal !== 0){
            timerVal = timerVal - 1

            setTimeout(() => {
                startResendTimer()
            }, 1000);
        } 
    }



    const sendOtpFunction = async () => {

        timerVal = 60

        try {
            const confirmation = await auth().signInWithPhoneNumber(number);
            setConfirm(confirmation);
        } catch (error) {
            console.log('====================================');
            console.log("error => ", error);
            console.log('====================================');
            alert(error);
        }
    }

    const verifyOtpFunction = async () => {
        try {
            var result = await confirm.confirm(otpValue);

            setConfirm(null);

            if(result){
                navigation.navigate('Home')
            }

        } catch (error) {
            console.log("error => ", error);
            alert('Invalid OTP');
        }
    }

   



    return (
        <SafeAreaView style={styles.container} >
            <View>
                <Image 
                    source={media.otp}
                    style={styles.loginImg}
                />
            </View>

            <View style={styles.contentContainer} >
                <Text style={styles.title} >OTP Verification</Text>
                <Text style={styles.description} >Please enter the 6 digit verification code send to <Text style={styles.numberText} >{number}</Text> </Text>

               
               <View>
                <OTPTextInput
                    ref={e => (otpInput = e)} 
                    tintColor={colors.black}
                    inputCount={6}
                    handleTextChange={(text) => {
                        setOtpValue(text)
                    }}
                    textInputStyle={{height: 40, width: 40, fontSize: 18}}
                />

               </View>
          
                <TouchableOpacity
                        onPress={() => {verifyOtpFunction()}}
                        disabled={false}
                        style={styles.loginButton}
                    >
                        <Text style={styles.loginButtonText} >VERIFY OTP</Text>
                </TouchableOpacity> 

                <View>
                    {resendTimer != 0
                        ?
                            <Text style={styles.resendTimer} >Resend OTP in {resendTimer} seconds</Text>
                        :
                            <TouchableOpacity
                                onPress={() => {sendOtpFunction(); startResendTimer();}}    
                            >
                                <Text style={styles.resendTimerText} >RESEND OTP</Text>
                            </TouchableOpacity>
                        }
                </View>
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        alignItems: 'center',
        padding: 20,
        justifyContent: 'space-between'
    },
    loginImg: {
        height: 250,
        width: 250,
        marginTop: 100,
        margin: 20,
    },
    contentContainer: {
        padding: 20, 
        alignItems: 'center', 
        width: screenWidth
    },
    numberText: {
        fontWeight: 'bold',
        color: colors.black,
        textDecorationLine: 'underline',
    },
    title: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 6,
    },
    description: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.gray,
        marginBottom: 10,
    },
    loginButton: {
        height: 60,
        borderRadius: 30,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: colors.black,
    },
    loginButtonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
    },
    inputStyle: {
        height: 50,
        width: '100%',
        paddingLeft: 10,
        fontSize: 18, 
        color: colors.black
    },
    resendTimer: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.gray,
        marginTop: 10,
    },
    resendTimerText: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.black,
        fontWeight: 'bold',
        marginTop: 10,
        textDecorationLine: 'underline',
    }
})

export default VerificationScreen
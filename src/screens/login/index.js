import React, {useState, useEffect} from 'react'
import { Text, View, TextInput,  StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity } from 'react-native'

import auth from '@react-native-firebase/auth';
import { colors } from '../../utils/colors';
import { media } from '../../utils/media';
import { screenWidth } from '../../utils/constants';


const LoginScreen = ({navigation}) => {

    useEffect(() => {
        auth().onAuthStateChanged((user) => {
            if(user) {
                console.log('====================================');
                console.log("user authenticated => ", user);
                console.log('====================================');
            } else {
                console.log('====================================');
                console.log("user not authenticated => ", user);
                console.log('====================================');
            }
          })
 
    }, [])
    
    
    const [number, setNumber] = useState('');
    const [otpValue, setOtpValue] = useState('');

    const [confirm, setConfirm] = useState(null);

    const sendOtpFunction = async() => {
        console.log("sendOtpFunction");

        var phoneNumber = '+91'+number

        navigation.navigate('Verification', {params: phoneNumber})
    }


    return (
        <SafeAreaView style={styles.container} >
            <View>
                <Image 
                    source={media.login}
                    style={styles.loginImg}
                />
            </View>

            <View style={styles.contentContainer} >
                <Text style={styles.title} >Enter your phone number</Text>
                <Text style={styles.description} >You will recieve a 6 digit code for phone number verification</Text>

                <View style={styles.inputContainer} >
                    <View style={styles.countryCode} >
                        <Text style={styles.countryCodeText} >+91</Text>
                    </View>
                    
                    <View style={styles.divider} />
                    
                    <View >
                        <TextInput
                            placeholder='Phone Number'
                            style={styles.inputStyle}
                            value={number}
                            onChangeText={(text) => setNumber(text)}
                        />
                    </View>

                  

                </View>
          
                <TouchableOpacity
                        onPress={() => {sendOtpFunction()}}
                        disabled={false}
                        style={styles.loginButton}
                    >
                        <Text style={styles.loginButtonText} >Send OTP</Text>
                </TouchableOpacity> 
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        alignItems: 'center',
        padding: 20,
        justifyContent: 'space-between'
    },
    loginImg: {
        height: 250,
        width: 250,
        marginTop: 100,
        margin: 20,
    },
    contentContainer: {
        padding: 20, 
        alignItems: 'center', 
        width: screenWidth,
    },
    title: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 6,
    },
    description: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.gray,
        marginBottom: 10,
    },
    loginButton: {
        height: 60,
        borderRadius: 30,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: colors.black,
    },
    loginButtonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
    },
    divider: {
        backgroundColor: colors.gray, 
        width: 2, 
        height: 30,
    },
    inputContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginVertical: 20, 
        marginBottom: 40, 
        backgroundColor: colors.light_gray, 
        borderWidth: 1, 
        borderRadius: 10, 
        width: '100%',
    },
    inputStyle: {
        height: 50,
        width: '100%',
        paddingLeft: 10,
        fontSize: 18, 
        color: colors.black
    },
    countryCode: {
        height: 50, 
        width: 50, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    countryCodeText: {
        fontSize: 18, 
        color: colors.black,
    },
})

export default LoginScreen
import React, { useEffect,useState } from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../screens/home';
import LoginScreen from '../screens/login';
import VerificationScreen from '../screens/login/VerficationScreen';


const Stack = createStackNavigator();

export default function Navigator() {


    return(
        <>
          <Stack.Navigator initialRouteName="Home" >
                <Stack.Screen 
                    name="Login" 
                    component={LoginScreen} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Verification" 
                    component={VerificationScreen} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Home" 
                    component={HomeScreen} 
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </>
    )
}

